Huffman coding in Assembly
=


After compilation, this program can be run with:

    compress.exe input_name output_name (-c | -d)

This program can do both compression or decompression

    -c compression
    -d decompression