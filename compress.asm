.model small

cseg            segment byte public 'CODE'
   assume  cs:cseg, ds:dseg, es:dseg, ss:sseg   

start:             


;::::::::::::::::::::::
;Parse command line
;:::::::::::::::::::::::

tab      equ     09h

mov     ax, seg dseg
mov     es, ax          


mov     si, 81h          
lea     di, FileName   
 SkipDelimiters:
lodsb                   
cmp     al, ' '
je      SkipDelimiters
cmp     al, ','
je      SkipDelimiters
cmp     al, Tab
je      SkipDelimiters
cmp     al, ';'
je      SkipDelimiters
cmp     al, '='
je      SkipDelimiters

dec     si              
GetFName:     
lodsb
cmp     al, 0dh
je      GotName
cmp     al, ' '
je       GotName
cmp     al, ','
je       GotName
cmp     al, Tab
je       GotName
cmp     al, ';'
je       GotName
cmp     al, '='
je       GotName

stosb                   
jmp     GetFName


GotName:        
mov     byte ptr es:[di], 0


lea     di, FileName2    
SkipDelimiters2:
lodsb                  
cmp     al, ' '
je      SkipDelimiters2
cmp     al, ','
je      SkipDelimiters2
cmp     al, Tab
je      SkipDelimiters2
cmp     al, ';'
je      SkipDelimiters2
cmp     al, '='
je      SkipDelimiters2


dec     si              ;Point at 1st char of name
GetFName2:      
lodsb
cmp     al, 0dh
je      GotName2
cmp     al, ' '
je       GotName2
cmp     al, ','
je       GotName2
cmp     al, Tab
je       GotName2
cmp     al, ';'
je       GotName2
cmp     al, '='
je       GotName2

stosb                   
jmp     GetFName2


GotName2:  
mov     byte ptr es:[di], 0


                
SkipDelimiters3:
lodsb                  
cmp     al, ' '
je      SkipDelimiters3
cmp     al, ','
je      SkipDelimiters3
cmp     al, Tab
je      SkipDelimiters3
cmp     al, ';'
je      SkipDelimiters3
cmp     al, '='
je      SkipDelimiters3
                
cmp     al, '-'
je      Getswitch
                
Getswitch:               
lodsb
cmp al,'d'
je Decode
cmp al,'c'
je Encode

 mov     bx, es          ;Point DS at DSEG
 mov     ds, bx
mov dx,offset SwitchError 
mov ah,40h 		; function 40h - write file
mov bx,1 		; handle = 1 (screen)
mov cx,24 				
int 21h  	
mov ax,4C00h 	; exit
int 21h

;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;Compress section of the Program XXXXXXXXXXXXXXXXXXXX
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx

Encode:
 mov     bx, es          ;Point DS at DSEG
 mov     ds, bx

mov dx,OFFSET FileName 	
mov al,2 		
mov ah,3Dh 		
int 21h 		

mov Handle,ax 		
jc ErrorOpening 	

keepreading:

mov dx,offset Buffer 	; address of buffer in dx
mov bx,Handle 		
mov cx,1024		
mov ah,3Fh 		; function 3Fh - read from file
int 21h 		
inc Totalbytesin

mov EOF, ax

mov cx,1024		; length of string
mov si,OFFSET Buffer 	; DS:SI - address of string

	
NextChar:
lodsb 			
add al,0
sub bx,bx
mov bl,al
add bx,bx
inc Freq [bx]

loop NextChar

jc ErrorReading 	

cmp     EOF, 1024          ;EOF reached?
jne     EOF1
jmp keepreading

EOF1:

mov bx,Handle 		 
mov ah,3Eh 		; function 3Eh - close a file
int 21h 		


mov dx,offset FileName2 	 
xor cx,cx 		
mov ah,3Ch 		
int 21h 		

jc CreateError 			


mov dx,OFFSET FileName2 	 
mov al,2 		
mov ah,3Dh 		; function 3Dh -open a file
int 21h 		

mov Handle2,ax 		

;:::::::::::::::::;
;save Freq array
;::::::::::::::::::::;


mov dx,offset Freq   
mov bx,Handle2 	
mov cx,512 		
mov ah,40h 		
int 21h		

;::::::::::::::::::::
;Get Totalchar
;::::::::::::::::::::

mov cx,512		; length of string
mov bx,0	

Addtotal:
mov ax,Freq [bx]
add Totalchar,ax
inc bx
inc bx
loop Addtotal

cmp Totalchar,1024
ja Moveon

mov dx,offset Toosmall 
mov ah,40h 		
mov bx,1 		
mov cx,33 				
int 21h  	
mov ax,4C00h 	; exit
int 21h

;::::::::::::::::::::
;This create the tree
;::::::::::::::::::::

Moveon:

mov dx,512 

Getfreq:
sub ax,ax
mov cx,512
mov bx,0

Codeloop:

cmp Freq [bx],0
jne getIndex
jmp endcodeloop

getIndex:
cmp Comp,0
je firstIndex

mov ax,Freq [bx]
cmp FreqTemp,ax
jbe endcodeloop
mov FreqTemp,ax
mov IndexTemp,bx
jmp endcodeloop

firstindex:
mov Comp,1
mov ax,Freq [bx]
mov FreqTemp,ax
mov IndexTemp,bx

endcodeloop:
inc bx
inc bx
loop Codeloop

setindex:
  cmp Freq1,0
  jne setindex2
  
mov ax, FreqTemp
mov Freq1,ax
mov bx,IndexTemp
mov Index1,bx

    
mov ax,Totalchar  
cmp Freq1, ax     
je encodetext

mov bx,Index1
mov Freq [bx],0
jmp nextsetindex

setindex2:  

  mov ax,FreqTemp
  mov Freq2,ax
  mov bx,IndexTemp
  mov Index2,bx
  mov Freq [bx],0
  
nextsetindex:
  cmp Freq2,0
  jne Codetree
  mov Comp,0
  
jmp Getfreq


Codetree: 

  mov bx,Index1
  mov Parent [bx],dx
  push bx
  mov bx,dx  ;switch bx and dx
  pop dx
  mov ax,Freq1
  add Freq [bx],ax
  mov Upkid[bx],dx
  mov Freq1,0
  mov Index1,0

  mov dx,bx  
  
  mov bx,Index2
  mov Parent [bx],dx
  push bx
  mov bx,dx  ;switch bx and dx
  pop dx
  mov ax,Freq2
  add Freq [bx],ax
  mov Dwnkid[bx],dx
  mov Freq2,0
  mov Index2,0 
  mov Comp,0

  mov dx,bx

  inc dx
  inc dx
  
jmp Getfreq

encodetext:
;::::::::::::::::::::
;Encode the file
;::::::::::::::::::::
mov bx,Index1
mov Root,bx


mov dx,OFFSET FileName 	 
mov al,2 		; access mode - read and write
mov ah,3Dh 		; function 3Dh -open a file
int 21h 		

mov Handle,ax 		
jc ErrorOpening 	

keepreading2:

mov dx,offset Buffer 	
mov bx,Handle 		
mov cx,1024		; amount of bytes to be read
mov ah,3Fh 		; function 3Fh - read from file
int 21h 		

mov EOF,ax

mov cx,EOF		
mov si,OFFSET Buffer 	; DS:SI - address of string

NextChar2a:
lodsb 
sub ah,ah
add al,0
add ax,ax
mov bx,ax	
 		
encodeloop:   
 
  mov bx,Parent[bx]
  cmp Upkid[bx],ax
  je addazero 
  mov dx,1
  push dx 
  inc Bitcnt
  jmp bitgetdone
  
  addazero:

  mov dx,0
  push dx
  inc Bitcnt
  
  bitgetdone:
  mov ax, bx 

  cmp Root,bx
  je writealetter
  jmp encodeloop 

writealetterreturn:  
loop NextChar2a

cmp     EOF, 1024        ;EOF reached?
jne     EOF2

jmp keepreading2


;:::::::::::::;
;bits to byte
;:::::::::::::;

writealetter:

encodebyte:

  cmp Bytefull,8
  je bytetobuff
  
  cmp Bitcnt,0
  je encodebytedone
  
  pop dx
  shl B,1
  or B,dl
  dec Bitcnt
  inc Bytefull
jmp encodebyte

encodebytedone:
jmp writealetterreturn

;:::::::::::::;
;byte to buffer
;:::::::::::::;

bytetobuff:

push ax
push bx
mov bx,BuffIndex
mov al,B
mov Buffercoded [bx],al
inc BuffIndex


pop bx
pop ax
mov B,0
mov Bytefull,0

cmp BuffIndex,1024
je bufftofile

jmp writealetter


;:::::::::::::;
;buffer to file
;:::::::::::::;

bufftofile:

push cx
mov dx,offset Buffercoded   
mov bx,Handle2	
mov cx,1024	; bytes to be written
mov ah,40h 		; function 40h - write to file
int 21h
pop cx
mov Buffindex,0
inc Totalbytesout


jmp writealetter


EOF2:

mov ax, BuffIndex
mov dx,offset Buffercoded   
mov bx,Handle2	
mov cx,BuffIndex	
mov ah,40h 		; function 40h - write to file
int 21h


mov bx,Handle2 		; 
mov ah,3Eh 		; function 3Eh - close a file
int 21h 		

mov bx,Handle 		 
mov ah,3Eh 		; function 3Eh - close a file
int 21h 		

mov ax,Totalbytesout
add ax,5
cmp Totalbytesin,ax
jae last
 
mov dx,OFFSET Filename2 	 
mov ah,41h 		; function 41h - delete file
int 21h 		

mov dx,offset Toobig 
mov ah,40h 		; function 40h - write file
mov bx,1 		
mov cx,47 				
int 21h  	
mov ax,4C00h 	; exit
int 21h

last:

mov dx,offset Filemessage
mov ah,9 			
int 21h 			

xor ah,ah 	
int 16h 	
cmp al,'q'
jz final

final:
mov ax,3 	
int 10h 	

mov ax,4C00h 	; exit
int 21h


;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;Decompress section of the Program XXXXXXXXXXXXXXXXXXXX
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx
;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx

Decode:

  mov     bx, es          ;Point DS at DSEG
  mov     ds,bx

mov dx,OFFSET FileName 	 
mov al,2 		
mov ah,3Dh 		
int 21h 		

mov Handle,ax 	

jc ErrorOpening 	


;:::::::::::::::::;
;Load Freq array
;::::::::::::::::::::;


mov dx,offset Bufferword	
mov bx,Handle 		
mov cx,512		
mov ah,3Fh 		
int 21h 		

mov cx,512	; length of string
mov bx,0	

Loadfreq:
mov ax,Bufferword [bx]
mov Freq [bx],ax
inc bx
inc bx
loop Loadfreq

;::::::::::::::::::::
;Get Totalchar
;::::::::::::::::::::

mov cx,512		; length of string
mov bx,0	

Addtotald:
mov ax,Freq [bx]
add Totalchar,ax
inc bx
inc bx
loop Addtotald

;::::::::::::::::::::
;This create the tree
;::::::::::::::::::::

mov dx,512 

Getfreqd:
sub ax,ax
mov cx,512
mov bx,0

Codeloopd:

cmp Freq [bx],0
jne getIndexd
jmp endcodeloopd

getIndexd:
cmp Comp,0
je firstIndexd

mov ax,Freq [bx]
cmp FreqTemp,ax
jbe endcodeloopd
mov FreqTemp,ax
mov IndexTemp,bx
jmp endcodeloopd

firstindexd:
mov Comp,1
mov ax,Freq [bx]
mov FreqTemp,ax
mov IndexTemp,bx

endcodeloopd:
inc bx
inc bx
loop Codeloopd

setindexd:
  cmp Freq1,0
  jne setindex2d
  
mov ax, FreqTemp
mov Freq1,ax
mov bx,IndexTemp
mov Index1,bx
 
mov ax,Totalchar  
cmp Freq1, ax     
je predecodeandwrite

mov bx,Index1
mov Freq [bx],0
jmp nextsetindexd

setindex2d:  

  mov ax,FreqTemp
  mov Freq2,ax
  mov bx,IndexTemp
  mov Index2,bx
  mov Freq [bx],0
  
nextsetindexd:
  cmp Freq2,0
  jne Codetreed
  mov Comp,0
  
jmp Getfreqd


Codetreed: 

  mov bx,Index1
  mov Parent [bx],dx
  push bx
  mov bx,dx  ;switch bx and dx
  pop dx
  mov ax,Freq1
  add Freq [bx],ax
  mov Upkid[bx],dx
  mov Freq1,0
  mov Index1,0

  mov dx,bx  
  
  mov bx,Index2
  mov Parent [bx],dx
  push bx
  mov bx,dx  ;switch bx and dx
  pop dx
  mov ax,Freq2
  add Freq [bx],ax
  mov Dwnkid[bx],dx
  mov Freq2,0
  mov Index2,0 
  mov Comp,0

  mov dx,bx

  inc dx
  inc dx

  
jmp Getfreqd

;:::::::::::::::::;
;Begin decodeing
;::::::::::::::::::::; 

predecodeandwrite:


mov bx,Index1
mov Root,bx
mov Temproot,bx
mov Buffindex,1024

mov dx,OFFSET FileName2 	 
xor cx,cx 		
mov ah,3Ch 		
int 21h 		

mov Handle2,ax

decodenow:

jmp loadabit

Walktree:
cmp bx,Loadedbitscnt
je loadabit

mov cx,Loadedbitscnt

Walktreeloop:
cmp Loadedbits[bx],0
je gotoupkid

push bx
mov bx,TempRoot
mov dx,Dwnkid[bx]
mov TempRoot,dx
pop bx
jmp thisstepdone

gotoupkid:
push bx
mov bx,TempRoot
mov dx,Upkid[bx]
mov TempRoot,dx
pop bx

thisstepdone:
inc bx
loop Walktreeloop

cmp TempRoot,512
jb getletter

mov ax,Root
mov Temproot,ax
jmp Walktree

;:::::::::::::
;load a bit
;:::::::::::::

loadabit:
cmp Bitcnt,0
je loadabyte

  sub ah,ah
  mov al,128
  and al,B
  shl B,1

   shr ax,1
   shr ax,1
   shr ax,1
   shr ax,1
   shr ax,1
   shr ax,1
   shr ax,1
 
mov bx,Loadedbitscnt
mov Loadedbits[bx], al
inc Loadedbitscnt
dec Bitcnt
mov bx,0

jmp Walktree


;:::::::::::::
;load a byte
;:::::::::::::

loadabyte:
mov ax,EOF
cmp Buffindex,ax
je loadak

mov bx,Buffindex
mov al,Buffer[bx]
mov B,al
mov Bitcnt,8 
inc Buffindex

jmp loadabit


;:::::::::::::
;load a kilobyte
;:::::::::::::

loadak:

cmp EOF,1024
jb finally

mov dx,offset Buffer 	
mov bx,Handle 	
mov cx,1024		
mov ah,3Fh 		
int 21h 		

mov EOF,ax

mov Buffindex,0

jmp loadabyte


getletter:

shr Temproot,1
mov bx,BuffIndexc
mov dx,Temproot
mov Bufferwrite [bx],dl
inc BuffIndexc

cmp BuffIndexc,1024
je writeout


getmore:
mov bx,Root
mov Temproot,bx
mov Loadedbitscnt,0
sub bx,bx
jmp decodenow


;:::::::::::::::::;
;Output
;::::::::::::::::::::;

writeout:

mov dx,offset Bufferwrite   
mov bx,Handle2 	
mov cx,EOF    	
mov ah,40h 		
int 21h

mov BuffIndexc,0


jmp getmore


finally:

mov ax, BuffIndexc
mov dx,offset Bufferwrite   
mov bx,Handle2 	
mov cx,BuffIndexc    	; 
mov ah,40h 		
int 21h

mov bx,Handle 		 
mov ah,3Eh 		
int 21h 		

mov bx,Handle2 		 
mov ah,3Eh 		
int 21h 
		
mov dx,OFFSET Filename 	 
mov ah,41h 		
int 21h 		

mov dx,offset Filemessage
mov ah,9 			
int 21h 

xor ah,ah	
int 16h 	
mov ax,4C00h 	; exit
int 21h

;:::::::::::::::::::;
;Error Messages
;::::::::::::::::::::

ErrorOpening:
mov dx,offset OpenError  
jmp EndError

ErrorReading:
mov dx,offset ReadError 
jmp EndError

WriteError:
mov dx,offset WriteMessage 
jmp EndError

OpenError:
mov dx,offset OpenMessage 
jmp EndError

CreateError:
mov dx,offset CreateMessage 

EndError:
mov ah,9 			
int 21h 			
mov ax,4C01h 
int 21h 



cseg            ends

dseg            segment byte public 'data'

lasttime DB 0
EOF DW 1024
PSP word    ?
Position   word    0
CR equ 13
LF equ 10
FileName DB 64 dup (?) 	
FileName2 DB 64 dup (?)  
Totalchar DW  0
Comp byte 0
Index1 DW ?
Index2 DW ?
FreqTemp DW ?
IndexTemp DW ?
Freq1  DW 0
Freq2  DW 0
Handle DW ? 			 
Handle2   DW ? 	 
BuffIndex DW 0
Bytefull DB 0
Buffer DB 1024 dup (?) 	
Buffercoded DB 1024 dup (?) 	
BuffIndexc DW 0	
Bufferword DW 512 dup (?)
Buffersmall DB 16 dup (?)
Bufferwrite DB 1024 dup (?)
Freq DW 512 dup (0)
Upkid DW 512 dup (?)
Dwnkid DW 512 dup (?)
Parent DW 512 dup (?)
infile DB 256 dup (?)
Bitcnt DW ?
B DB 0
Root DW ?
Switch DW ?
TempRoot DW ?
Bytestowritecnt DW 0
Loadedbits DB 16 dup (?)
Loadedbitscnt DW 0
Totalbytesout DW  0
Totalbytesin DW  0


WriteMessage  DB "An error has occurred (WRITING)$"
OpenMessage   DB "An error has occurred (OPENING)$"
CreateMessage DB "An error has occurred (CREATING)$"
ReadError DB "An error has occured(reading)!$"
Filemessage DB "File Created, Press any key to continue.$"
SwitchError DB "Invalid switch provided."
Toosmall DB "File < 1kb, cannot be compressed."
Toobig DB "Output larger than input, cannot be compressed."

dseg            ends

sseg            segment byte stack 'stack'
  stk             word    0ffh dup (?)
 sseg            ends


zzzzzzseg       segment para public 'zzzzzz'
LastBytes       byte    16 dup (?)
zzzzzzseg       ends
END start